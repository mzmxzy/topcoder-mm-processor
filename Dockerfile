#SAMPLE DOCKERFILE TO GENERATE THE IMAGE FOR SCORER
FROM node:latest

COPY . /topcoder-mm-processor

WORKDIR /topcoder-mm-processor

RUN npm install

EXPOSE 3000

ENTRYPOINT ["npm", "run"]
CMD ["start"]
